﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace _01_TypesAndVariables
{
    [TestClass]
    public class ReferenceTypeExamples
    {
        [TestMethod]
        public void strings()
        {
            string declared;
            declared = "This is initialized";

            string declarationAndInitialization = "This is both declaring and initializing";


            // Concatenate

            string firstName = "Bobert";
            string lastName = "Nilliam";
            string space = " ";
            string concatenatedFullName = firstName + space + lastName;
            int age = 112;

            // Composite Formating
            string compositeFullName = string.Format("Hello, I am {0} {1}. Attorney at law.", firstName, lastName);

            // Interpolation
            string interpolationFullName = $"{lastName}, {firstName} {lastName}. I am {age}.";
            Console.WriteLine(concatenatedFullName);
            Console.WriteLine(compositeFullName);
            Console.WriteLine(interpolationFullName);
        }

        [TestMethod]
        public void Collections()
        {
            // Arrays 
            string stringExample = "Hello World";

            string[] stringArray = { "Hello", "World", "Why", "is it", "always", stringExample, "?" };

            string thirdItem = stringArray[2];
            Console.WriteLine(thirdItem);
            Console.WriteLine(stringArray[2]);
            stringArray[0] = "Hey there";
            Console.WriteLine(stringArray[0]);

            // Lists
            List<string> listOfString = new List<string>();
            List<int> listOfIntegers = new List<int>();
            listOfString.Add("42");
            listOfIntegers.Add(42);
            Console.WriteLine(listOfIntegers[0]);

            // Queues
            Queue<string> firstInFirstOut = new Queue<string>();
            firstInFirstOut.Enqueue("I am first");
            firstInFirstOut.Enqueue("I am next");
            string firstItem = firstInFirstOut.Dequeue();
            Console.WriteLine(firstItem);

            // Dictionary
            Dictionary<int, string> keyAndValue = new Dictionary<int, string>();
            keyAndValue.Add(7, "Agent");
            string valueSeven = keyAndValue[7];
            Console.WriteLine(valueSeven);

            // Other Examples
            SortedList<int, string> sortedKeyAndValue = new SortedList<int, string>();
            HashSet<int> uniqueList = new HashSet<int>();
            Stack<string> lastInFirstOut = new Stack<string>();

        }

        [TestMethod]
        public void Classes()
        {
            Random rng = new Random();
            int randomNumber = rng.Next(1, 100); //(Min, Max)
            Console.WriteLine(randomNumber);

        }

    }
}
