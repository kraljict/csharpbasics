﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_Classes
{
    public class Greeter
    {
       public void SayHello(string name)
        {
            Console.WriteLine($"Hello there, {name}");

        }

        public void SayHello()
        {
            Console.WriteLine("Hello Stranger");
        }

        Random _rando = new Random();

        public void GetRandomGreeting()
        {
            string[] availableGreeting = new string[] { "Hello", "Howdy", "Sup", "Hola", "Suh Dude", "Hi Y'all", "Guten Tag", "Ni Hao", "Yo Bro", "Waddup", "Hi"};
            int randomNumber = _rando.Next(0, availableGreeting.Length);
            string randoGreeting = availableGreeting.ElementAt(randomNumber);
            Console.WriteLine($"{randoGreeting}"); // or Console.WriteLine(randoGreeting);
        }



    }
}
