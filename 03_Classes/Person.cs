﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_Classes
{
   public class Person //This is a class
    {
        public string FirstName { get; set; } // do prop tab tab to do this

        private string _lastName;

        public string LastName //Properties are pascal casing.. All properties have a backing field. 
        {

            get { return _lastName; } 
            set { _lastName = value; }
        }

        public DateTime DateOfBirth { get; set; }
        public Vehicle Transport { get; set; }
        
        public string FullName
        {
            get
            {
                return $"{FirstName} {LastName}"; // If I never use set, I can never access this outside. Readonly property is without set.
            
            }
        }

        public int  Age
        {
            get
            {
                TimeSpan ageSpan = DateTime.Now - DateOfBirth;
                double totalAgeInYears = ageSpan.TotalDays / 365.25;
                int yearsOfAge = Convert.ToInt32(Math.Floor(totalAgeInYears));
                return yearsOfAge;
            }
        }
        
        public Person() { } // Type Class
        public Person(string firstName, string lastName, DateTime dob, Vehicle transport) //Green text signifies class
        {
            FirstName = firstName;
            LastName = lastName;
            DateOfBirth = dob;
            Transport = transport;
            //This is an overload constructor
        }

    }
}
