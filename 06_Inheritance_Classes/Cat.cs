﻿using System;

namespace _06_Inheritance_Classes
{
    public class Cat : Animal // Only able to inherit from one class
    {
        public double ClawLength { get; set; }

        public Cat()
        {
            Console.WriteLine("This is the Cat Constructor.");
            IsMammal = true;
            TypeOfDiet = DietType.Carnivore;
        }

        public override void Move() // All Methods are Pascal Case like Move and Cat
        {
            Console.WriteLine($"The {GetType().Name} moves quickly.");
        }

        public virtual void MakeSound()
        {
            Console.WriteLine("Meow.");
        }
    }

    public class Liger : Cat
    {
        public Liger()
        {
            Console.WriteLine("This is the Liger Constructor.");
        }

        public override void Move()
        {
            Console.WriteLine($"The {GetType().Name} stalks its prey");
            base.Move();
        }

        public override void MakeSound()
        {
            Console.WriteLine("Roar.");
        }
    }
}
