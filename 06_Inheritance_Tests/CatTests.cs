﻿using System;
using _06_Inheritance_Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace _06_Inheritance_Tests
{
    [TestClass]
    public class CatTests
    {
        [TestMethod]
        public void CatTesting()
        {
            Animal firstAnimal = new Animal();
            Cat firstCat = new Cat();
            firstAnimal.Move(); //Add paranthesis to methods not properties.
            firstCat.Move();
            firstCat.MakeSound();

            Liger oneLiger = new Liger();
            oneLiger.Move();
            oneLiger.MakeSound();


        }
    }
}
