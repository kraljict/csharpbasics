﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_RepositoryPattern_Repository
{
    public class StreamingRepository : StreamingContentRepository
    {
        public Show GetShowByTitle(string title)
        {
            foreach (StreamingContent content in _contentDirectory)
            {
                if (content.Title.ToLower() == title.ToLower() && content.GetType() == typeof(Show))
                {
                    return (Show)content;
                }
            }
            return null;
        }

        public Movie GetMovieByTitle(string title)
        {
            foreach (StreamingContent content in _contentDirectory)
            {
                if (content.Title.ToLower() == title.ToLower() && content.GetType() == typeof(Movie))
                {
                    return (Movie)content;
                }
            }

            return null;
        }

        public List<Show> GetAllShows()
        {
            //Make a space to save all shows
            List<Show> allShows = new List<Show>();
            //Pull one item and see if it is a show
            //Make sure to save that off to the side
            foreach(StreamingContent content in _contentDirectory)
            {
                if(content is Show)
                {
                    allShows.Add((Show)content);
                }
            }
            //Return a list
            return allShows;
        }

        //Write GetAllMovies
        public List<Movie> GetAllMovies()
        {
            //Make a space to save all shows
            List<Movie> allMovies = new List<Movie>();
            //Pull one item and see if it is show
            //Make sure to save that off to the side
            foreach(StreamingContent content in _contentDirectory)
            {
                if(content is Movie)
                {
                    allMovies.Add((Movie)content);
                }
            }
            //Return a List
            return allMovies;
        }
        //GetByOtherParamaters ex: GetAllFamilyFriendlyMovies
        // Get Shows with over x episode

        //Going to pass in a value (x) that
        //single out all shows form my list(aka fake db)
        //now i have a list of shows
        //use a paramter Epsidoes to get episode count
        //using that number compared to the number passed in, add it to a list
        //Return a list

        public List<Show> GetAllShowsOverACertainEpisodeCount(int episodeCount) //Create Method
        {
            List<Show> finalList = new List<Show>(); // Create New List
            var listOfAllShows = GetAllShows();
            foreach(var eachShow in listOfAllShows)
            {
                if(eachShow.Episodes.Count() >= episodeCount)
                {
                    finalList.Add(eachShow);
                }
            }
            return finalList;
        }


        


    }
}
