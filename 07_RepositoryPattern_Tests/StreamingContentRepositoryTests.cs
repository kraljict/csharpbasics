﻿using System;
using System.Collections.Generic;
using _07_RepositoryPattern_Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace _07_RepositoryPattern_Tests
{
    [TestClass]
    public class StreamingContentRepositoryTests
    {
        [TestMethod]
        public void AddToDirectory_ShouldGetCorrectBoolean()
        {
            //Triple A Paradigm: Methodology to build a test. It's a testing pattern.

           // ARRANGE: Setting up playing field that you will be testing
            StreamingContent content = new StreamingContent();
            StreamingContentRepository repository = new StreamingContentRepository();

           // ACT: This is where you will run the code you are testing
            bool addResult = repository.AddContentToDirectory(content);

           // ASSERT: Did this method do what it was supposed to?
            Assert.IsTrue(addResult);


        }

        [TestMethod]
        public void GetDirectory_ShouldReturnCorrectCollection()
        {
            //ARRANGE
            StreamingContent newObject = new StreamingContent();
            StreamingContentRepository repo = new StreamingContentRepository();
            repo.AddContentToDirectory(newObject);

            //ACT
            List<StreamingContent> listOfContent = repo.GetContents();

            //ASSERT
            bool directoryHasContent = listOfContent.Contains(newObject);
            Assert.IsTrue(directoryHasContent);
        }

        private StreamingContentRepository _repo;
        private StreamingContent _content;
        [TestInitialize]
        public void Arrange()
        {
            _repo = new StreamingContentRepository();
            _content = new StreamingContent("Oceans 8", "do crime?", 100, MaturityRating.NC_17,
              GenreType.Action);
            _repo.AddContentToDirectory(_content);
        }

        [TestMethod]
        public void GetByTitle_ShouldReturnCorrectContent()
        {
            //ARRANGE: Already Completed with Arrange() Method


            //ACT
            StreamingContent searchResult = _repo.GetContentByTitle("Oceans 8");


            //ASSERT
            Assert.AreEqual(_content, searchResult);
        }

        [TestMethod]
        public void UpdateExistingContent_ShouldReturnTrue()
        {
            //Arrange
            StreamingContent updatedContent = new StreamingContent("Italian Job", "do crime?", 100,
                MaturityRating.NC_17, GenreType.Action);

            //ACT
            bool updateResult = _repo.UpdateExistingContent("oceans 8", updatedContent);

            //ASSERT
            Assert.IsTrue(updateResult);
        }

        [TestMethod]
        public void DeleteExistingContent_ShouldReturnTrue()
        {
            //ARRANGE
            StreamingContent foundContent = _repo.GetContentByTitle("Oceans 8");
            //ACT
            bool removeResult = _repo.DeleteExistingContent(foundContent);
            //ASSERT
            Assert.IsTrue(removeResult);

        }

    }
}
