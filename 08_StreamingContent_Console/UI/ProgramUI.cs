﻿using _07_RepositoryPattern_Repository;
using _10_StreamingContent_UIRefactor;
using System;
using System.Collections.Generic;
using System.Net.Mime;

namespace _08_StreamingContent_Console.UI
{
    class ProgramUI
    {
        private readonly IConsole _console;
        private readonly StreamingRepository _streamingRepo = new StreamingRepository(); //Fields are variables but with a default class wide scope.
        public ProgramUI(IConsole console)
        {
            _console = console;
        }
        public void Run()
        {
            SeedContent();//This will add all data to db before running. 
            RunMenu();//This will run the application.
        }

        private void RunMenu()
        {
            bool continueToRun = true;
            while (continueToRun)
            {

                //Get all shows
                //Get all movies

                Console.Clear();
                Console.WriteLine("Enter the number of the option you'd like to select: \n" +
                    "1) Show all streaming content \n" +
                    "2) Find by title \n" +
                    "3) Add new content \n" +
                    "4) Remove content \n" +
                    "5) View all movies \n" +
                    "6) View all shows \n" +
                    "7) Exit");
                string userInput = Console.ReadLine();
                switch (userInput)
                {
                    case "1":
                        //Show All
                        ShowAllContent();
                        break;
                    case "2":
                        //Find by Title
                        ShowContentByTitle();
                        break;
                    case "3":
                        //Add New
                        CreateNewContent();
                        break;
                    case "4":
                        //Remove
                        RemoveContentFromList();
                        break;
                    case "5":
                        //View all movies
                        ShowAllMovies();
                        break;
                    case "6":
                        //View all shows
                        ViewAllShows();
                        break;
                    case "7":
                        //Exit
                        continueToRun = false;
                        break;
                    
                    default:
                        //default
                        Console.WriteLine("Please enter a valid number between 1 and 5. \n" +
                            "Press any key to continue......");
                        Console.ReadKey();
                        break;
                }
            }

        }
        private void CreateNewContent()
        {
            // a new content object 
            StreamingContent content = new StreamingContent();
            //Ask user for information 
            //Title
            Console.WriteLine("Please enter the title of the new content");
            string contentTitle = Console.ReadLine();
            content.Title = contentTitle;
            //Description
            Console.WriteLine($"Please enter the description for {content.Title}");
            content.Description = Console.ReadLine();
            //StarRating
            Console.WriteLine($"Please enter the star rating for {content.Title}");
            content.StarRating = float.Parse(Console.ReadLine());
            //Maturity Rating
            Console.WriteLine("Select a Maturity Rating: \n" +
                "1) G \n" +
                "2) PG \n" +
                "3) PG 13 \n" +
                "4) R \n" +
                "5) NC 17 \n" +
                "6) MA");
            string maturityResponse = Console.ReadLine();
            switch (maturityResponse)
            {
                case "1":
                    content.MaturityRating = MaturityRating.G;
                    break;
                case "2":
                    content.MaturityRating = MaturityRating.PG;
                    break;
                case "3":
                    content.MaturityRating = MaturityRating.PG_13;
                    break;
                case "4":
                    content.MaturityRating = MaturityRating.R;
                    break;
                case "5":
                    content.MaturityRating = MaturityRating.NC_17;
                    break;
                case "6":
                    content.MaturityRating = MaturityRating.MA;
                    break;
            }
            //TypeOfGenre
            Console.WriteLine("Select a genre: \n" +
                "1) Horror \n" +
                "2) RomCom \n" +
                "3) Fantasy \n" +
                "4) Sci-Fi \n" +
                "5) Drama \n" +
                "6) Bromance \n" +
                "7) Action \n" +
                "8) Documentary \n" +
                "9) Thriller");
            string genreResponse = Console.ReadLine();
            int genreID = int.Parse(genreResponse);
            content.TypeOfGenre = (GenreType)genreID;

            //a new content with properties filled out by user 
            //Pass that to the add method in our repo 
            _streamingRepo.AddContentToDirectory(content);
        }
        private void ShowAllContent()
        {
            Console.Clear();
            //Get all items in our fake database
            List<StreamingContent> listOfContent = _streamingRepo.GetContents();
            //Take EACH item and display property values
            foreach (StreamingContent content in listOfContent)
            {
                DisplaySimple(content);
            }
            //Pause the program so the user can see the printed objects
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            //Show all items in our fake database

        }
        private void ShowContentByTitle()
        {
            Console.Clear();
            Console.WriteLine("Please enter the title:");
            string title = Console.ReadLine();
            StreamingContent foundContent = _streamingRepo.GetContentByTitle(title);
            if (foundContent != null)
            {
                DisplayAllProps(foundContent);
            }
            else
            {
                Console.WriteLine("There are no titles that match your search");
            }

            Console.WriteLine("Press any key to continue....");
            Console.ReadKey();
        }
        private void ShowAllMovies()
        {
            Console.Clear();

            List<Movie> movies = _streamingRepo.GetAllMovies();

            foreach(Movie content in movies)
            {
                DisplayAllProps(content);
            }
            Console.WriteLine("Press any key to go back...");
            Console.ReadKey();
        }
        private void ViewAllShows()
        {
            List<Show> shows = _streamingRepo.GetAllShows();

            foreach(Show content in shows)
            {
                DisplaySimple(content);
            }
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

       
        private void RemoveContentFromList()
        {
            // Ask the user which one they want to remove
            Console.WriteLine("Which item would you like to remove?");
            // Need a list of the items
            List<StreamingContent> contentList = _streamingRepo.GetContents();
            int count = 0;
            foreach (var content in contentList)
            {
                count++;
                Console.WriteLine($"{count}) {content.Title}");
            }
            // Take in user response 
            int targetContentID = int.Parse(Console.ReadLine());
            int correctIndex = targetContentID - 1;
            if (correctIndex >= 0 && correctIndex < contentList.Count)
            {
                StreamingContent desiredContent = contentList[correctIndex];
                if (_streamingRepo.DeleteExistingContent(desiredContent))
                {
                    Console.WriteLine($"{desiredContent.Title} successfully removed!");
                }
                else
                {
                    Console.WriteLine("I'm sorry. I'm afraid I can't do that.");
                }
            }
            else
            {
                Console.WriteLine("INVALID OPTION");
            }
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            // Remove that item
        }
        private void DisplaySimple(StreamingContent content)
        {
            Console.WriteLine($"{content.Title} \n" +
                    $"{content.Description} \n" +
                    $"-----------------------");
        }
        private void DisplayAllProps(StreamingContent content)
        {
            Console.WriteLine($"Title: {content.Title} \n" +
                $"Description: {content.Description} \n" +
                $"Genre: {content.TypeOfGenre} \n" +
                $"Stars: {content.StarRating} \n" +
                $"Family Friendly: {content.isFamilyFriendly} \n" +
                $"Maturity Rarigin: {content.MaturityRating} \n");
        }

      
        private void SeedContent()
        {
            var titleOne = new StreamingContent("Toy Story", "Toys have a story", 4.5f, MaturityRating.PG, GenreType.Bromance);

            var titleTwo = new StreamingContent("Star Wars", "Stars at war", 10f,  MaturityRating.PG, GenreType.Documentary);

            var titleThree = new StreamingContent("Harry Potter", "Harry makes pottery", 4.5f, MaturityRating.PG,GenreType.Action);

            var titleFour = new StreamingContent("Baby Driver", "Your driver is a baby", 2.5f, MaturityRating.PG,GenreType.Drama);

            var titleFive = new Movie("Toy Story Two", "Toys have a story continued", 3.5f, MaturityRating.PG, GenreType.Bromance, 1.21);

            var testMovie = new Movie("Test Movie", "Test Description", 3.8f, MaturityRating.R, GenreType.Bromance, 1.00);

            _streamingRepo.AddContentToDirectory(titleOne);
            _streamingRepo.AddContentToDirectory(titleTwo);
            _streamingRepo.AddContentToDirectory(titleThree);
            _streamingRepo.AddContentToDirectory(titleFour);
            _streamingRepo.AddContentToDirectory(titleFive);
            _streamingRepo.AddContentToDirectory(testMovie);

        }

    }
}