﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_Interfaces_Introduction
{
    public class Banana : IFruit
    {
        public string Name { get { return "Banana"; } }
        public bool IsPeeled { get; private set; }
        public string Peel()
        {
            IsPeeled = true;
            return "You peel the banana";
        }

    }

    public class Orange: IFruit
    {
        public string Name { get { return "Orange"; } }
        public bool IsPeeled { get; private set; }

        public Orange(){}
        public Orange(bool isPeeled)
        {
            IsPeeled = isPeeled;
        }

        public string Peel()
        {
            IsPeeled = true;
            return "You peel the orange";
        }
        public string Squeeze()
        {
            return "You squeeze the orange and juice comes out";
        }
    }

    public class Grape : IFruit
    {
        public string Name { get { return "Grape"; } }

        public bool IsPeeled { get; } = false;

        public string Peel()
        {
            return "Who peels grapes?";
        }
    }

    public class StarFruit : IFruit
    {
        public string Name { get { return "StarFruit"; } }

        public bool IsPeeled { get; private set; } 

        public string Peel()
        {
            IsPeeled = true;
            return "StarFruit is delicious when peeled!";
        }
    }

    public class Pomegranate : IFruit
    {
        public string Name { get { return "Pomegranate"; } }
        public bool IsPeeled { get; private set; }
        public string Peel()
        {
            IsPeeled = true;
            return "You can't eat a pomegranate if it is not peeled!";
        }
    }

    public class Watermelon : IFruit
    {
        public string Name { get { return "Watermelon"; } }
        public bool IsPeeled { get; } = false;

        public string Peel()
        {
            return "You can't peel a watermelon. You have to cut it up!";
        }
    }

    public class Strawberry : IFruit
    {
        public string Name { get { return "Strawberry"; } }
        public bool IsPeeled { get; } = false;
        public string Peel()
        {
            return "You can't peel a strawberry.";
        }
    }

    public class Tangerine: IFruit
    {
        public string Name { get { return "Strawberry"; } }
        public bool IsPeeled { get; private set; }
        public string Peel()
        {
            IsPeeled = true;
            return "Tangerines are delicious but you have to peel them first!";
        }
    }

    public class Lemon: IFruit
    {
        public string Name { get { return "Lemon"; } }
        public bool IsPeeled { get; private set; }

        public string Peel()
        {
            IsPeeled = true;
            return "Lemons are very sour!";
        }
    }

    public class Lime:IFruit
    {
        public string Name { get { return "Lime"; } }
        public bool IsPeeled { get; } = true;

        public string Peel()
        {
            return "Limes are very bitter!";
        }
    }

    public class Apple: IFruit
    {
        public string Name { get { return "Apple"; } }
        public bool IsPeeled { get; private set; }
        public string Peel()
        {
            IsPeeled = true;
            return "Apples are great when peeled!";
        }
    }
}
