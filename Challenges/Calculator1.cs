﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenges
{
    public class Calculator1
    {
        public int AddTwoNumbers(int numOne, int numTwo)
        {
            int sum = numOne + numTwo;
            return sum;
        }

        public int SubtractTwoNumbers(int NumOne, int NumTwo)
        {
            int difference = NumOne - NumTwo;
            return difference;
        }

        public int MultiplyTwoNumbers(int NumOne, int NumTwo)
        {
            int Multiply = NumOne * NumTwo;
            return Multiply;
        }

        public int DivideTwoNumbers(int numOne, int numTwo)
        {
            int Divide = numOne / numTwo;
            return Divide;
        }

        public float AddTwoNumbers(float numOne, float numTwo)
        {

            float sum = numOne + numTwo;
            return sum;
        }

        public float SubtractTwoNumbers(float numOne, float numTwo)
        {
            float sum = numOne - numTwo;
            return sum;
        }

    }
}
