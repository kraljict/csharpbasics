﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Challenges
{
    [TestClass]
    public class CalculatorTest
    {
        [TestMethod]
        public void SumOfAddTwoNumbers()
        {
            //Arrange
            Calculator1 calculator1 = new Calculator1();

            //Act
            int expected = 10;
            var actual = calculator1.AddTwoNumbers(3,7);
            Console.WriteLine(actual);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        public void DifferenceOfTwoNumbers()
        {
            //Arrange
            Calculator1 calculator1 = new Calculator1();

            //Act
            int expected = -3;
            var actual = calculator1.SubtractTwoNumbers(3, 5);
            Console.WriteLine(actual);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        public void MultiplyOfTwoNumbers()
        {
            //Arrange
            Calculator1 calculator1 = new Calculator1();

            //Act
            int expected = 10;
            var actual = calculator1.MultiplyTwoNumbers(2, 5);
            Console.WriteLine(actual);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        public void DivideOfTwoNumbers()

        {
            //Arrange
            Calculator1 calculator1 = new Calculator1();

            //Act
            int expected = 2;
            var actual = calculator1.MultiplyTwoNumbers(10, 5);
            Console.WriteLine(actual);

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
