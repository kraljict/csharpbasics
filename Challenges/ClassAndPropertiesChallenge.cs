﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenges
{
    public class ClassAndPropertiesChallenge
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public DateTime birthDay { get; set; }
        public int ID {get;}

        public ClassAndPropertiesChallenge()
        {

        }
        public void Person(string firstName, string lastName, DateTime birthDay, int ID)
        {
            firstName = firstName;
            lastName = lastName;
            birthDay = birthDay;
            ID = ID;
            
        }
 
    }

      
}
