﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Challenges
{
    [TestClass]
    public class ClassChallenges
    {
        [TestMethod]
        public void ClassChallenge()
        {
            string challengeWord = "Supercalifragilisticexpialidocious";
            foreach (char letter in challengeWord)
            {
                if (letter == 'i' || letter == 'l')
                {
                    Console.WriteLine(letter);
                }

                else
                {
                    Console.WriteLine("Not an i");
                }
            }

            int numberOfLetters = challengeWord.Length;

            Console.WriteLine(numberOfLetters);

        }
    }
}
